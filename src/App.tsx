import React, { useEffect } from "react";
import "fontsource-roboto";
import { NavBar } from "./RouterNavBar/NavBar";
import { useStore } from "./Stores/store";
import { BrowserRouter as Router } from "react-router-dom";

export function App() {
  const { booksStore, usersStore, authorsStore } = useStore();

  useEffect(() => {
    booksStore.fetchBooks();
    usersStore.fetchUsers();
    authorsStore.fetchAuthors();
  });

  return (
    <Router>
      <NavBar />
    </Router>
  );
}
