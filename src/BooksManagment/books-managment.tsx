import React, { useState } from "react";
import { InfoBox } from "../InfoBox/info-box";
import { useStore } from "../Stores/store";
import { IconButton, makeStyles, Divider, Typography } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { observer } from "mobx-react-lite";
import { EditButton } from "../EditButton/edit-button";

const useStyles = makeStyles({
  books: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    marginTop: "3vh",
    marginLeft: "5vw",
  },
  page: {
    display: "flex",
    direction: "rtl",
  },
  users: {
    display: "flex",
    flexDirection: "column",
    marginRight: "5vw",
    marginTop: "3vh",
    marginLeft: "5vw",
  },
});

export const BooksManagment = observer(() => {
  const classes = useStyles();

  const { usersStore, booksStore, authorsStore } = useStore();
  const [selectedBookId, setSelectedBookId] = useState<number>();

  const usersToDisplay = () => {
    return usersStore.data.filter((user) =>
      user.readBookIds.includes(selectedBookId as number)
    );
  };

  const getAuthorName = (authorId: number) =>
    authorsStore.data.find((author) => author.id === authorId)?.name;

  const editBook = (id: number) => (newBookName: string) => {
    booksStore.editBook(id, newBookName);
  };

  const getBookName = () =>
    booksStore.data.find((book) => book.id === selectedBookId)?.name;

  const booksInfoElements = () => {
    return booksStore.data.map(({ id, name, authorId }) => (
      <InfoBox
        key={id}
        id={id}
        name={name}
        author={getAuthorName(authorId)}
        onClick={setSelectedBookId}
      >
        <EditButton textToEdit={name} onEdit={editBook(id)} />
        <IconButton onClick={() => booksStore.removeBook(id)}>
          <DeleteIcon />
        </IconButton>
      </InfoBox>
    ));
  };

  const usersInfoElements = () => {
    return usersToDisplay().map(({ id, name }) => (
      <InfoBox key={id} id={id} name={name}>
        <IconButton
          onClick={() =>
            usersStore.removeReadBookFromUser(id, selectedBookId as number)
          }
        >
          <DeleteIcon />
        </IconButton>
      </InfoBox>
    ));
  };

  return (
    <div className={classes.page}>
      <div className={classes.books}>{booksInfoElements()}</div>
      <Divider orientation="vertical" flexItem />
      <div className={classes.users}>
        {selectedBookId !== undefined && (
          <Typography>{`הקוראים של ${getBookName()}:`}</Typography>
        )}
        {usersInfoElements()}
      </div>
    </div>
  );
});
