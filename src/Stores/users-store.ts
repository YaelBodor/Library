import { action, decorate, observable } from "mobx";
import { users, User } from "../data";

export class UsersStore {
  data: User[] = [];

  fetchUsers() {
    Promise.resolve(users).then((data) => {
      this.data = data;
    });
  }

  removeUser(userIdToRemove: number) {
    this.data = this.data.filter((user) => user.id !== userIdToRemove);
  }

  editUserName(editedUserId: number, newName: string) {
    this.data = this.data.map((user) =>
      editedUserId === user.id ? { ...user, name: newName } : user
    );
  }

  changeUsersFavoriteBook(editedUserId: number, newFavoriteBookId: number) {
    this.data = this.data.map((user) =>
      editedUserId === user.id
        ? { ...user, favoriteBookId: newFavoriteBookId }
        : user
    );
  }

  addReadBookToUser(editedUserId: number, newReadBookId: number) {
    this.data = this.data.map((user) =>
      editedUserId === user.id
        ? { ...user, readBookIds: [...user.readBookIds, newReadBookId] }
        : user
    );
  }

  removeReadBookFromUser(editedUserId: number, readBookIdToRemove: number) {
    this.data = this.data.map((user) =>
      editedUserId === user.id
        ? {
            ...user,
            readBookIds: [
              ...user.readBookIds.filter(
                (bookId) => bookId !== readBookIdToRemove
              ),
            ],
          }
        : user
    );
  }
}

decorate(UsersStore, {
  data: observable,
  fetchUsers: action,
  removeUser: action,
  editUserName: action,
  changeUsersFavoriteBook: action,
  addReadBookToUser: action,
  removeReadBookFromUser: action,
});
