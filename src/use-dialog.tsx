import React from "react";

export function useDialog(): [boolean, () => void, () => void] {
  const [isDialogOpen, setIsDialogOpen] = React.useState(false);

  const showDialog = () => setIsDialogOpen(true);
  const hideDialog = () => setIsDialogOpen(false);

  return [isDialogOpen, showDialog, hideDialog];
}
