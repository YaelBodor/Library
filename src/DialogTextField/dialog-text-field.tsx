import React from "react";
import { TextField, InputAdornment } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";

interface DialogTextFieldProps {
  editNameText: string;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export function DialogTextField({
  editNameText,
  handleInputChange,
}: DialogTextFieldProps) {
  return (
    <TextField
      autoFocus
      margin="dense"
      id="name"
      type="text"
      fullWidth
      value={editNameText}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <EditIcon />
          </InputAdornment>
        ),
      }}
      onChange={handleInputChange}
    />
  );
}
