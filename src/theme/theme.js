import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    secondary: {
      main: "#ea80fc",
    },
  },
});
