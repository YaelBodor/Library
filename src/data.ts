export const books = [
  { id: 0, name: "לא רציונלי ולא במקרה", authorId: 0 },
  { id: 1, name: "קיצור תולדות האנושות", authorId: 1 },
  { id: 2, name: "הארי פוטר", authorId: 2 },
];

export const users = [
  { id: 0, name: "עידן דוידי", readBookIds: [0, 1], favoriteBookId: 1 },
  { id: 1, name: "אודי דורון", readBookIds: [0] },
  { id: 2, name: "אלכס קוביצה", readBookIds: [0, 1, 2], favoriteBookId: 1 },
];

export const authors = [
  { id: 0, name: "דן אריאלי" },
  { id: 1, name: "יובל נח הררי" },
  { id: 2, name: "ג. ק. רולינג" },
];

export interface Book {
  id: number;
  name: string;
  authorId: number;
}

export interface User {
  id: number;
  name: string;
  readBookIds: number[];
  favoriteBookId?: number;
}

export interface Author {
  id: number;
  name: string;
}
