import React, { useState } from "react";
import { InfoBox } from "../InfoBox/info-box";
import { useStore } from "../Stores/store";
import { IconButton, makeStyles, Divider, Typography } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { observer } from "mobx-react-lite";
import { EditButton } from "../EditButton/edit-button";

const useStyles = makeStyles({
  authors: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    marginTop: "3vh",
    marginLeft: "5vw",
  },
  page: {
    display: "flex",
    direction: "rtl",
  },
  books: {
    display: "flex",
    flexDirection: "column",
    marginRight: "5vw",
    marginTop: "3vh",
    marginLeft: "5vw",
  },
});

export const AuthorsManagment = observer(() => {
  const classes = useStyles();

  const { booksStore, authorsStore } = useStore();
  const [selectedAuthorId, setSelectedAuthorId] = useState<number>();

  const booksToDisplay = () => {
    return booksStore.data.filter(
      (book) => book.authorId === (selectedAuthorId as number)
    );
  };

  const editAuthor = (id: number) => (newBookName: string) => {
    authorsStore.editAuthor(id, newBookName);
  };

  const getAuthorName = () =>
    authorsStore.data.find((author) => author.id === selectedAuthorId)?.name;

  const authorsInfoElements = () => {
    return authorsStore.data.map(({ id, name }) => (
      <InfoBox key={id} id={id} name={name} onClick={setSelectedAuthorId}>
        <EditButton textToEdit={name} onEdit={editAuthor(id)} />
        <IconButton onClick={() => authorsStore.removeAuthor(id)}>
          <DeleteIcon />
        </IconButton>
      </InfoBox>
    ));
  };

  const usersInfoElements = () => {
    return booksToDisplay().map(({ id, name }) => (
      <InfoBox key={id} id={id} name={name} />
    ));
  };

  return (
    <div className={classes.page}>
      <div className={classes.authors}>{authorsInfoElements()}</div>
      <Divider orientation="vertical" flexItem />
      <div className={classes.books}>
        {selectedAuthorId !== undefined && (
          <Typography>{`הספרים של ${getAuthorName()}:`}</Typography>
        )}
        {usersInfoElements()}
      </div>
    </div>
  );
});
