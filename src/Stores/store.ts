import { createContext, useContext } from "react";
import { BooksStore } from "./books-store";
import { UsersStore } from "./users-store";
import { AuthorsStore } from "./authors-store";

export interface Store {
  booksStore: BooksStore;
  usersStore: UsersStore;
  authorsStore: AuthorsStore;
}

export const store: Store = {
  booksStore: new BooksStore(),
  usersStore: new UsersStore(),
  authorsStore: new AuthorsStore(),
};

export const storeContext = createContext(store);

export const useStore = () => {
  return useContext(storeContext);
};
