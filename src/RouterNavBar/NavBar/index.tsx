import React from "react";
import {
  Drawer,
  makeStyles,
  Theme,
  AppBar,
  CssBaseline,
  Toolbar,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import MenuBookIcon from "@material-ui/icons/MenuBook";
import CreateIcon from "@material-ui/icons/Create";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import { UserManagment } from "../../UsersManagement/users-management";
import { Route, Link, Switch } from "react-router-dom";
import { BooksManagment } from "../../BooksManagment/books-managment";
import { AuthorsManagment } from "../../AuthorsManagment/authors-managment";

const useStyles = makeStyles((theme: Theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    direction: "rtl",
  },
  drawer: {
    // width: "100%",
    flexShrink: 0,
  },
  drawerPaper: {
    width: "20vw",
  },
  drawerContainer: {
    overflow: "auto",
  },
  text: {
    textAlign: "right",
    color: "black",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginRight: "21vw",
    direction: "rtl",
  },
}));

export function NavBar() {
  const classes = useStyles();

  return (
    <div>
      <CssBaseline />
      <AppBar position="sticky" className={classes.appBar} color="secondary">
        <Toolbar>
          <img src="logo.png" alt="logo"></img>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="right"
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            <Link to="/users" style={{ textDecoration: "none" }}>
              <ListItem button className={classes.text}>
                <ListItemIcon>{<PermIdentityIcon />}</ListItemIcon>
                <ListItemText primary={"ניהול משתמשים"} />
              </ListItem>
            </Link>
            <Link to="/books" style={{ textDecoration: "none" }}>
              <ListItem button className={classes.text}>
                <ListItemIcon>{<MenuBookIcon />}</ListItemIcon>
                <ListItemText primary={"ניהול ספרים"} />
              </ListItem>
            </Link>
            <Link to="/authors" style={{ textDecoration: "none" }}>
              <ListItem button className={classes.text}>
                <ListItemIcon>{<CreateIcon />}</ListItemIcon>
                <ListItemText primary={"ניהול סופרים"} />
              </ListItem>
            </Link>
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Switch>
          <Route path="/users" component={UserManagment} />
          <Route path="/books" component={BooksManagment} />
          <Route path="/authors" component={AuthorsManagment} />
        </Switch>
      </main>
    </div>
  );
}
