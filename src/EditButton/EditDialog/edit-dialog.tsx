import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
import { DialogTextField } from "../../DialogTextField/dialog-text-field";

interface EditDialogProps {
  isOpen: boolean;
  value: string;
  closeDialog: () => void;
  onConfirm: (newValue: string) => void;
}

export function EditDialog({
  isOpen,
  value,
  closeDialog,
  onConfirm,
}: EditDialogProps) {
  const [editText, setEditText] = React.useState(value);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEditText(event.currentTarget.value);
  };

  const cancel = () => {
    closeDialog();
    setEditText(value);
  };

  const confirm = () => {
    if (editText) {
      onConfirm(editText);
      closeDialog();
    } else {
      cancel();
    }
  };

  return (
    <Dialog open={isOpen} onClose={cancel}>
      <DialogTitle>Edit</DialogTitle>
      <DialogContent>
        <DialogTextField
          editNameText={editText}
          handleInputChange={handleInputChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={cancel} color="primary">
          Cancel
        </Button>
        <Button onClick={confirm} color="primary">
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
}
