import React from "react";
import { IconButton } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import { useDialog } from "../use-dialog";
import { EditDialog } from "./EditDialog/edit-dialog";

interface EditProps {
  textToEdit: string;
  onEdit: (newValue: string) => void;
}

export function EditButton({ textToEdit, onEdit }: EditProps) {
  const [isDialogOpen, showDialog, hideDialog] = useDialog();

  return (
    <div>
      <IconButton onClick={showDialog}>
        <EditIcon />
      </IconButton>
      <EditDialog
        isOpen={isDialogOpen}
        value={textToEdit}
        closeDialog={hideDialog}
        onConfirm={onEdit}
      />
    </div>
  );
}
