import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Card, CardActions, CardContent, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    minWidth: "24vw",
    width: "20vw",
    display: "flex",
    direction: "rtl",
    margin: 0,
    justifyContent: "space-between",
    marginBottom: "2vh",
  },
  title: {
    fontSize: "12px",
  },
  content: {
    cursor: (canClick) => (canClick ? "pointer" : "default"),
  },
});

interface InfoBoxProps {
  id: number;
  name: string;
  author?: string;
  children?: React.ReactNode;
  onClick?: React.Dispatch<React.SetStateAction<number | undefined>>;
}

export function InfoBox({ id, name, author, children, onClick }: InfoBoxProps) {
  const classes = useStyles(onClick !== undefined);

  return (
    <Card className={classes.root}>
      <CardContent
        onClick={() => onClick && onClick(id)}
        className={classes.content}
      >
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {`מזהה: ${id}`}
        </Typography>
        <Typography variant="h5" component="h2">
          {`שם: ${name}`}
        </Typography>
        {author && (
          <Typography color="textSecondary">{`סופר: ${author}`}</Typography>
        )}
      </CardContent>
      <CardActions>{children}</CardActions>
    </Card>
  );
}
