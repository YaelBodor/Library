import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App";
import { theme } from "./theme/theme";
import { ThemeProvider } from "@material-ui/core";
import { store, storeContext } from "./Stores/store";

ReactDOM.render(
  <storeContext.Provider value={store}>
    <ThemeProvider theme={theme}>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </ThemeProvider>
  </storeContext.Provider>,
  document.getElementById("root")
);
