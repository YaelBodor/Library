import { action, decorate, observable } from "mobx";
import { authors, Author } from "../data";

export class AuthorsStore {
  data: Author[] = [];

  fetchAuthors() {
    Promise.resolve(authors).then((data) => {
      this.data = data;
    });
  }

  removeAuthor(authorIdToRemove: number) {
    this.data = this.data.filter((author) => author.id !== authorIdToRemove);
  }

  editAuthor(id: number, newName: string) {
    this.data = this.data.map((author) =>
      author.id === id ? { ...author, name: newName } : author
    );
  }
}

decorate(AuthorsStore, {
  data: observable,
  fetchAuthors: action,
  removeAuthor: action,
  editAuthor: action,
});
