import { action, decorate, observable } from "mobx";
import { books, Book } from "../data";

export class BooksStore {
  data: Book[] = [];

  fetchBooks() {
    Promise.resolve(books).then((data) => {
      this.data = data;
    });
  }

  removeBook(bookIdToRemove: number) {
    this.data = this.data.filter((book) => book.id !== bookIdToRemove);
  }

  editBook(id: number, newName: string) {
    this.data = this.data.map((book) =>
      book.id === id ? { ...book, name: newName } : book
    );
  }
}

decorate(BooksStore, {
  data: observable,
  fetchBooks: action,
  removeBook: action,
  editBook: action,
});
