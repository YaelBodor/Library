import React, { useState } from "react";
import { InfoBox } from "../InfoBox/info-box";
import { useStore } from "../Stores/store";
import {
  IconButton,
  makeStyles,
  Divider,
  Checkbox,
  Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import StarIcon from "@material-ui/icons/Star";
import { observer } from "mobx-react-lite";
import { EditButton } from "../EditButton/edit-button";

const useStyles = makeStyles({
  users: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    marginTop: "3vh",
    marginLeft: "5vw",
  },
  page: {
    display: "flex",
    direction: "rtl",
  },
  books: {
    display: "flex",
    flexDirection: "column",
    marginRight: "5vw",
    marginTop: "3vh",
    marginLeft: "5vw",
  },
});

export const UserManagment = observer(() => {
  const classes = useStyles();
  const { usersStore, booksStore, authorsStore } = useStore();
  const [selectedUserId, setSelectedUserId] = useState<number>();

  const booksToDisplay = () => {
    const selectedUser = usersStore.data.find(
      (user) => user.id === selectedUserId
    );

    return booksStore.data.filter((book) =>
      selectedUser?.readBookIds.includes(book.id)
    );
  };

  const isFavoriteBook = (bookId: number) => {
    const selectedUser = usersStore.data.find(
      (user) => user.id === selectedUserId
    );

    return selectedUser?.favoriteBookId === bookId;
  };

  const getAuthorName = (authorId: number) =>
    authorsStore.data.find((author) => author.id === authorId)?.name;

  const editUser = (id: number) => (newUserName: string) => {
    usersStore.editUserName(id, newUserName);
  };

  const getUserName = () =>
    usersStore.data.find((user) => user.id === selectedUserId)?.name;

  const usersInfoElements = () => {
    return usersStore.data.map(({ id, name }) => (
      <InfoBox key={id} id={id} name={name} onClick={setSelectedUserId}>
        <EditButton textToEdit={name} onEdit={editUser(id)} />
        <IconButton onClick={() => usersStore.removeUser(id)}>
          <DeleteIcon />
        </IconButton>
      </InfoBox>
    ));
  };

  const booksInfoElements = () => {
    return booksToDisplay().map(({ id, name, authorId }) => (
      <InfoBox key={id} id={id} name={name} author={getAuthorName(authorId)}>
        <Checkbox
          icon={<StarBorderIcon />}
          checkedIcon={<StarIcon />}
          checked={isFavoriteBook(id)}
          onClick={() =>
            usersStore.changeUsersFavoriteBook(selectedUserId as number, id)
          }
        />
        <IconButton
          onClick={() =>
            usersStore.removeReadBookFromUser(selectedUserId as number, id)
          }
        >
          <DeleteIcon />
        </IconButton>
      </InfoBox>
    ));
  };

  return (
    <div className={classes.page}>
      <div className={classes.users}>{usersInfoElements()}</div>
      <Divider orientation="vertical" flexItem />
      <div className={classes.books}>
        {selectedUserId !== undefined && (
          <Typography>{`הספרים שקרא ${getUserName()}:`}</Typography>
        )}
        {booksInfoElements()}
      </div>
    </div>
  );
});
